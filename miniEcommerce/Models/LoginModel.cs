﻿using miniEcommerce.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace miniEcommerce.Models
{
    public class LoginModel : BaseModel
    {//public User GetLogin(string username , string password)
        public UserModel GetLogin(string username , string password)
        {
            try
            {
                UserModel userModel = new UserModel();
                var result = db.Users.Where(x => x.Username.ToLower().Contains(username.ToLower()) &&
                                                x.Password.ToLower().Contains(MD5Hash(password).ToLower())).FirstOrDefault();
                userModel.User = result;
                return userModel;
            }
            catch (Exception ex)
            {
                return new UserModel
                {
                    StatusCode = 500,
                    StatusMessage = ex.Message
                };
            }
        }
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text  
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it  
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
    }


}