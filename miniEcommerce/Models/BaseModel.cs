﻿using miniEcommerce.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace miniEcommerce.Models
{
    public class BaseModel
    {
        public TrainningMVCDataContext db = new TrainningMVCDataContext();
       
    }
    public class UserModel
    {
        public int StatusCode { get; set; } = 200;
        public string StatusMessage { get; set; }
        public User User { get; set; }
    }

    public class AllProductModel
    {
        public int StatusCode { get; set; } = 200;
        public string StatusMessage { get; set; }
        public Product Product { get; set; }
        public Category Category { get; set; }
        public SelectList DropDownList { get; set; }
    }
    public class GetCategoryName
    {
        public string Name { get; set; }
       
    }

    public class GetProductModel
    {
        public int StatusCode { get; set; } = 200;
        public string StatusMessage { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public double Cost { get; set; }
        public double Prict { get; set; }
        
    }

    public class GetDetailProductModel
    {
        public int StatusCode { get; set; } = 200;
        public string StatusMessage { get; set; }
        public List<GetProductModel> category { get; set; } = new List<GetProductModel>();
    }

}