﻿using miniEcommerce.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace miniEcommerce.Models
{
    public class Register : BaseModel
    {
        public UserModel getRegister(string name , string username, string password)
        {
            try
            {
                UserModel userModel = new UserModel();
                DateTime dateTime = DateTime.Now;
                var insert = new User();
                var result = db.Users.Where(x => x.Username.ToLower().Contains(username.ToLower())).FirstOrDefault();
                if(result == null)
                {                   
                    insert.Name = name;
                    insert.Username = username;
                    insert.Password = MD5Hash(password);
                    insert.CreateDate = dateTime;
                    db.Users.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    userModel.User = insert;
                    return userModel;
                }
                userModel.StatusCode = 500;
                return userModel;
            }
            catch (Exception ex)
            {
                return new UserModel
                {
                    StatusCode = 500, 
                    StatusMessage = ex.Message
                };
            }
        }
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text  
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it  
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }

            return strBuilder.ToString();
        }
    }
   
}