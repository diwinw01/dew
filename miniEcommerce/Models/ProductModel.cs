﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using miniEcommerce.DataContext;

namespace miniEcommerce.Models
{
    public class ProductModel : BaseModel
    {
        public AllProductModel AddProduct(string name , int category , int cost , int price)
        {
            try
            {
                DateTime dateTime = DateTime.Now;
                AllProductModel allProductModel = new AllProductModel();
                var insert = new Product();
                insert.Name = name;
                insert.Category = category;
                insert.Cost = cost;
                insert.Price = price;
                insert.CreateDate = dateTime;               
                db.Products.InsertOnSubmit(insert);
                db.SubmitChanges();
                allProductModel.Product = insert;
                return allProductModel;
            }
            
            catch (Exception ex)
            {
                return new AllProductModel
                {
                    StatusCode = 501,
                    StatusMessage = ex.Message
                };
            }
        }

        public AllProductModel AddCategory(string name)
        {
            try
            {                
                AllProductModel allProductModel = new AllProductModel();
                var result = db.Categories.Where(x => x.Category1.ToLower().Contains(name.ToLower())).FirstOrDefault();
                if(result == null)
                {
                    var insert = new Category();
                    insert.Category1 = name;
                    db.Categories.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    allProductModel.Category = insert;
                    return allProductModel;
                }
                allProductModel.StatusCode = 500;
                return allProductModel; 
            }

            catch (Exception ex)
            {
                return new AllProductModel
                {
                    StatusCode = 501,
                    StatusMessage = ex.Message
                };
            }
        }

        public GetDetailProductModel GetProduct()
        {
            GetDetailProductModel getProduct = new GetDetailProductModel();
            try
            {
                
                BaseModel baseModel = new BaseModel();
                var result = (from a in baseModel.db.Products
                             join b in baseModel.db.Categories on a.Category equals b.Id
                             select new GetProductModel
                             {
                                 Name = a.Name,
                                 Id = a.Id,
                                 Category = b.Category1,
                                 Cost = (a.Cost ?? 0),
                                 Prict = (a.Price ?? 0)
                             }).ToList();               
                if(result != null)
                {
                    getProduct.category = result;
                    return getProduct;
                }
                return new GetDetailProductModel
                {
                    StatusCode = 500,
                    
                };

            }
            catch (Exception ex)
            {                
                return new GetDetailProductModel
                {
                    StatusCode = 501,
                    StatusMessage = ex.Message
                };

            }
            
            
        }
    }
}