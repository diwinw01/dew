﻿using miniEcommerce.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace miniEcommerce.Controllers
{
    public class HomeController : Controller
    {
        ProductModel productModel = new ProductModel();
        public ActionResult Index()
        {

            try
            {
                BaseModel baseModel = new BaseModel();
                //var name = from a in baseModel.db.Categories
                //           select ( x => new SelectListItem()
                //           {
                //               Text = baseModel.db.Categories.ToString(),
                //               Value = baseModel.db.Categories.ToString()
                //           }).ToList();
                //)

                var result = baseModel.db.Categories.Select(i => new SelectListItem()
                {
                    Text = i.Category1,
                    Value = i.Id.ToString()
                }).ToList();

                ViewData["category"] = result;


                return View();
            }
            catch (Exception ex)
            {

                return View("About");
            }




        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Product()
        {
            try
            {
                var result = productModel.GetProduct();
                if (result.category != null)
                    return View(result.category);

                return View("About");
            }
            catch (Exception)
            {

                return View("About");
            }

        }

        public ActionResult ProductJtable()
        {
            return View();
        }


        public JsonResult AddProduct(string name, int category, int cost, int price)
        {
            try
            {

                var result = productModel.AddProduct(name, category, cost, price);
                if (result.StatusCode == 200 && result.Product != null)
                {
                    return Json(new { product = result, status = 200 });
                }

                return Json(new { statusCode = result.StatusCode, statusMessage = result.StatusMessage });
            }
            catch (Exception ex)
            {

                return Json(new { statusCode = 400, statusMessage = ex.Message });
            }
        }

        public JsonResult AddCategory(string name)
        {
            try
            {
                var result = productModel.AddCategory(name);
                if (result.StatusCode == 200 && result.Category != null)
                {
                    return Json(new { category = result, status = 200 });
                }

                return Json(new { statusCode = result.StatusCode, statusMessage = result.StatusMessage });
            }
            catch (Exception ex)
            {

                return Json(new { statusCode = 400, statusMessage = ex.Message });
            }
        }
        [HttpPost]
        public JsonResult ProductList()
        {
            try
            {

                var result = productModel.GetProduct();
                return Json(new { status = 200, product = result.category });
            }
            catch (Exception ex)
            {
                return Json(new { status = 500, statucMessage = ex.Message });
            }
        }
    }
}