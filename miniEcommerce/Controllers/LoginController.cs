﻿using miniEcommerce.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace miniEcommerce.Controllers
{
    public class LoginController : BaseController
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }

        // GET: Login/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Login/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Login/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Login/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Login/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Login/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Login/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public JsonResult GetLogin(string username, string password)
        {
            try
            {
                LoginModel loginModel = new LoginModel();
                var result = loginModel.GetLogin(username, password);

                if (result.StatusCode == 200 && result.User != null)
                {
                    return Json(new { user = result, status = 200 });
                }

                return Json(new { statusCode = 500, statusMessage = result.StatusMessage });


            }
            catch (Exception ex)
            {

                return Json(new { statusCode = 400, statusMessage = ex.Message });
            }
        }
        public JsonResult GetRegister(string name, string username, string password)
        {
            try
            {
                Register register = new Register();
                var result = register.getRegister(name, username, password);
                if (result.StatusCode == 200)
                    return Json(new { user = result.User, status = 200 });

                return Json(new { statusCode = 500, statusMessage = result.StatusMessage });
            }
            catch (Exception ex)
            {
                return Json(new { statusCode = 400, statusMessage = ex.Message });

            }
        }
    }
}
